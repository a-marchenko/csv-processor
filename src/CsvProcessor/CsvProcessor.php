<?php
namespace Am\CsvProcessor;

class CsvProcessor
{
    private $delimiter = ',';
    private $enclosure = '"';
    private $escape = '"';
    private $callback;
    protected $header;
    protected $file;
    protected $parsedContent;

    /**
     * CsvParser constructor.
     *
     * @param resource $file
     */
    public function __construct($file)
    {
        if (!is_resource($file)) {
            throw new \InvalidArgumentException('File must be a resource');
        }
        $this->file = $file;
    }

    /**
     * @param resource $file
     * @return CsvProcessor
     */
    public static function buildFromResource($file): self
    {
        return new static($file);
    }

    /**
     * @param string $file
     * @return CsvProcessor
     */
    public static function buildFromFile(string $file): self
    {
        if (!file_exists($file)) {
            throw new \InvalidArgumentException(sprintf('File %s does not exists', var_export($file, true)));
        }

        return new static(fopen($file, 'r'));
    }

    /**
     * @return array
     */
    public function parseHeader(): array
    {
        if (empty($this->getHeader())) {
            $header = $this->readCsvLine();
            if (empty($header)) {
                throw new \RuntimeException('Cannot parse header');
            }
            $this->header = array_map($this->getCallback(), $header);
        }
        return $this->getHeader();
    }

    /**
     * @param null|callable $callback
     * @return array
     */
    public function parseContent(callable $callback = null): array
    {
        if (is_null($this->parsedContent)) {
            $oldPointer = $this->getPointer();
            $this->reset();
            $this->parsedContent = $this->walk(
                function ($associativeRow, $line, $pointer) use ($callback) {
                    return is_callable($callback)
                        ? call_user_func_array($callback, [$associativeRow, $line, $pointer])
                        : $associativeRow;

                },
                true
            );
            $this->setPointer($oldPointer);
        }

        return $this->parsedContent;
    }

    /**
     * @param callable $callback
     * @param bool     $isSaveCallbackResult
     * @return array|null
     */
    public function walk(callable $callback, bool $isSaveCallbackResult = false): ?array
    {
        $header = $this->reset()->parseHeader();
        $result = null;
        $parsedLine = 0;
        $pointer = $this->getPointer();

        while (($row = $this->readCsvLine()) !== false) {
            $parsedLine++;
            $row = array_map($this->getCallback(), $row);
            if (count($header) !== count($row)) {
                throw new \RuntimeException(
                    sprintf(
                        'Invalid CSV file at line: `%s`, the number of columns '
                            . 'in the header(%s) and row(%s) should be equal',
                        $parsedLine,
                        count($header),
                        count($row)
                    )
                );
            }
            $params = [array_combine($header, $row), $parsedLine, $pointer];
            $pointer = $this->getPointer();
            if ($isSaveCallbackResult) {
                $callbackLastResult = call_user_func_array($callback, $params);
                $result[] = $callbackLastResult;
            } else {
                $callbackLastResult = call_user_func_array($callback, $params);
            }
            if ($callbackLastResult === false) {
                break;
            }
        }

        return $result;
    }

    /**
     * @return array|bool
     */
    protected function readCsvLine()
    {
        return fgetcsv(
            $this->getFileHandle(),
            0,
            $this->getDelimiter(),
            $this->getEnclosure(),
            $this->getEscape()
        );
    }

    /**
     * @return resource
     */
    protected function getFileHandle()
    {
        return $this->file;
    }

    /**
     * @return string
     */
    protected function getDelimiter(): string
    {
        return $this->delimiter;
    }

    /**
     * @param string $delimiter
     * @return CsvProcessor
     */
    public function setDelimiter(string $delimiter): self
    {
        $this->delimiter = $delimiter;
        return $this;
    }

    /**
     * @return string
     */
    protected function getEnclosure(): string
    {
        return $this->enclosure;
    }

    /**
     * @param string $enclosure
     * @return CsvProcessor
     */
    public function setEnclosure(string $enclosure): self
    {
        $this->enclosure = $enclosure;
        return $this;
    }

    /**
     * @return string
     */
    protected function getEscape(): string
    {
        return $this->escape;
    }

    /**
     * @param string $escape
     * @return CsvProcessor
     */
    public function setEscape(string $escape): self
    {
        $this->escape = $escape;
        return $this;
    }

    /**
     * @return callable
     */
    public function getCallback(): callable
    {
        if (!is_callable($this->callback)) {
            $this->callback = function ($value) {
                return $value;
            };
        }

        return $this->callback;
    }

    /**
     * @param callable $callback
     * @return CsvProcessor
     */
    public function setCallback(callable $callback): self
    {
        $this->callback = $callback;
        return $this;
    }

    /**
     * @return CsvProcessor
     */
    public function reset(): self
    {
        $this->header = null;
        rewind($this->getFileHandle());

        return $this;
    }

    public function getHeader()
    {
        return $this->header;
    }

    /**
     * @param $pointer
     * @return CsvProcessor
     */
    protected function setPointer(int $pointer): self
    {
        fseek($this->getFileHandle(), $pointer);
        return $this;
    }

    /**
     * @return int
     */
    public function getPointer(): int
    {
        return ftell($this->getFileHandle());
    }

    /**
     * @param int $pointer
     * @return array
     */
    public function getRowByPointer(int $pointer): array
    {
        $oldPointer = $this->getPointer();
        $header = $this->reset()->parseHeader();
        $line = $this->setPointer($pointer)->readCsvLine();
        $result = array_combine($header, $line);
        $this->setPointer($oldPointer);

        return $result;
    }
}
