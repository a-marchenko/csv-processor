<?php

use Am\CsvProcessor\CsvProcessor;
use \org\bovigo\vfs\vfsStream;

class CsvProcessorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function csvProcessTest()
    {
        $csvContent = <<<CSV
a,b,c
1,1,1
2,2,2
3,3,3
4,4,4
5,5,5
6,6,6
CSV;

        $fileName = $this->generateCsvFile($csvContent);
        $csvRowLength = mb_strpos($csvContent, PHP_EOL) + mb_strlen(PHP_EOL);

        $csvProcessor = CsvProcessor::buildFromFile($fileName);

        $expectedHeader = ['a', 'b', 'c'];
        $csvHeaderResult = $csvProcessor->parseHeader();

        // first csv line - header
        $this->assertEquals($expectedHeader, $csvHeaderResult);

        // Reading second line of csv data
        $this->assertEquals($csvProcessor->getRowByPointer(2 * $csvRowLength), ['a' => 2, 'b' => 2, 'c' => 2]);

        // Pointer must be after header
        $this->assertEquals($csvProcessor->getPointer(), $csvRowLength);

        $csvLinesResult = $csvProcessor->walk(
            function($row, $line, $pointer) use ($csvRowLength) {
                $this->assertEquals($row['a'], $line);
                $this->assertEquals($line * $csvRowLength, $pointer);
                return implode(',', $row);
            },
            true
        );

        // is `walk` works for all lines
        $this->assertEquals(implode(',', $csvHeaderResult) . "\n". implode("\n", $csvLinesResult), $csvContent);

        // after `walk` pointer must be at the end of a file
        $pointer = $csvProcessor->getPointer();
        $this->assertEquals($pointer, mb_strlen($csvContent));

        // Reading third line of csv data
        $this->assertEquals($csvProcessor->getRowByPointer(3 * $csvRowLength), ['a' => 3, 'b' => 3, 'c' => 3]);

        $parsedContent = $csvProcessor->parseContent();

        $this->assertEquals(['a' => 1, 'b' => 1, 'c' => 1], $parsedContent[0]);
        $this->assertEquals(['a' => 3, 'b' => 3, 'c' => 3], $parsedContent[2]);
        $this->assertEquals(['a' => 6, 'b' => 6, 'c' => 6], $parsedContent[5]);

        $this->assertEquals($csvProcessor->getPointer(), $pointer);

        $csvProcessor->walk(
            function($row, $line, $pointer) use ($csvRowLength) {
                $this->assertEquals($row['b'], $line);
                $this->assertEquals($line * $csvRowLength, $pointer);
                if ($row['a'] == 4) {
                    return false;
                }
            }
        );

        $this->assertEquals(5 * $csvRowLength, $csvProcessor->getPointer());

        $parsedContent = $csvProcessor->parseContent();
        $this->assertEquals(['a' => 1, 'b' => 1, 'c' => 1], $parsedContent[0]);

        $this->assertEquals(5 * $csvRowLength, $csvProcessor->getPointer());

    }

    /**
     * @test
     */
    public function buildFromFile()
    {
        $csvFileName = $this->generateCsvFile("a|b|c\n1|1|`bla`");
        $csvProcessor = CsvProcessor::buildFromFile($csvFileName)
            ->setDelimiter('|')
            ->setEnclosure('`')
            ->setEscape('`')
            ->setCallback(function($value) {
                $result = $value;
                if (ctype_digit($value)) {
                    $result = $value * 3;
                }
                return $result;
            });

        $parsedContent = $csvProcessor->parseContent();
        $this->assertEquals(1, count($parsedContent));
        $this->assertEquals(['a' => 3, 'b' => 3, 'c' => 'bla'], $parsedContent[0]);
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage File 'some' does not exists
     */
    public function invalidFileTest()
    {
        CsvProcessor::buildFromFile('some');
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage File must be a resource
     */
    public function invalidResourceTest()
    {
        CsvProcessor::buildFromResource('some');
    }

    /**
     * @test
     * @expectedException \RuntimeException
     * @expectedExceptionMessage Cannot parse header
     */
    public function emptyFileTest()
    {
        CsvProcessor::buildFromFile($this->generateCsvFile(''))->parseContent();
    }

    /**
     * @test
     * @expectedException \RuntimeException
     * @expectedExceptionMessage Invalid CSV file at line: `2`, the number of columns in the header(3) and row(4) should be equal
     */
    public function invalidCsvFileTest()
    {
        $csvContent = <<<CSV
a,b,c
1,1,1
2,2,2,2
CSV;
        CsvProcessor::buildFromFile($this->generateCsvFile($csvContent))->parseContent();
    }

    private function generateCsvFile(string $content): string
    {
        $virtualRootDir = vfsStream::setup('csv');
        $onlyName = uniqid('csv-');
        $csvFileName = vfsStream::url($virtualRootDir->getName() . "/{$onlyName}.csv");

        $file = fopen($csvFileName, 'a');
        fwrite($file, $content);
        fclose($file);

        return $csvFileName;
    }

}